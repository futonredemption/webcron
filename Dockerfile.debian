FROM node:8.11.4-stretch

RUN apt-get update \
  && apt-get install -y gconf-service libasound2 libatk1.0-0 libc6 libcairo2 \
      libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 \
      libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 \
      libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
      libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 \
      libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation \
      libappindicator1 libnss3 lsb-release xdg-utils wget \
  && rm -rf /var/cache/apk/*
RUN useradd --create-home --home-dir /home/webcron --shell /bin/bash webcron

RUN chown -R webcron /home/webcron

WORKDIR /home/webcron
USER webcron

COPY --chown=webcron:webcron . .
RUN npm install --save
RUN npm install --only=dev
RUN npm run build
RUN npm run test

ENV PORT 3000
EXPOSE 3000

ENTRYPOINT ["npm", "run"]
CMD ["serve"]

LABEL maintainer="jeremyje@gmail.com"
LABEL version="1.1.0"
LABEL description="A server that will periodically download web content and store it in text, PDF, JPEG, and PNG format."

ARG BUILD_DATE
ARG VCS_REF

# http://label-schema.org/rc1/
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="webcron"
LABEL org.label-schema.description="A server that will periodically download web content and store it in text, PDF, JPEG, and PNG format."
LABEL org.label-schema.usage="README.md"
LABEL org.label-schema.url="http://www.futonredemption.com"
LABEL org.label-schema.vcs-url="https://bitbucket.org/futonredemption/webcron.git"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vendor="Futon Redemption"
LABEL org.label-schema.version="1.1.0"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.docker.cmd="docker run -d --name webcron --interactive --tty -p 18080:3000 webcron:latest"
LABEL org.label-schema.docker.debug="docker exec -it $CONTAINER npm run serve --help"
LABEL org.label-schema.docker.cmd.help="docker exec -it $CONTAINER npm run serve --help"
